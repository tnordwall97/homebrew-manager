#!/bin/bash
apt update -y
curl -sSL https://repos.insights.digitalocean.com/install.sh | bash
apt install -y \
    docker.io \
    docker-compose \
    &&

systemctl enable docker
systemctl start docker
docker network create nginx-proxy

docker run -d -p 9001:9001 --name portainer_agent --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/docker/volumes:/var/lib/docker/volumes portainer/agent

mkdir -p dockers/nginx
curl https://gitlab.com/HomebrewSoft/homebrew-manager/-/raw/master/nginx-proxy-le.yml -o dockers/nginx/docker-compose.yml
docker-compose -f dockers/nginx/docker-compose.yml up -d

mkdir -p dockers/local
curl https://gitlab.com/HomebrewSoft/homebrew-manager/-/raw/master/odoo-postgres.yml -o dockers/local/docker-compose.yml
vim dockers/local/docker-compose.yml &&
docker-compose -f dockers/local/docker-compose.yml up -d
