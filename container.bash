#!/bin/bash
apt update -y
apt install git vim -y &&

pip3 install wheel &&

curl https://gitlab.com/HomebrewSoft/homebrewsh/odoo-instances/-/raw/server/templates/13.0/config/odoo.conf > /etc/odoo/odoo.conf
echo admin_passwd=$(openssl rand -base64 16) >> /etc/odoo/odoo.conf &&
tail -n 1 /etc/odoo/odoo.conf

chown -R odoo:odoo /etc/odoo
chown -R odoo:odoo /mnt/extra-addons
